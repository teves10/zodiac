<!DOCTYPE html>
<html>
<head>
	<title>Zodiac</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/slate/bootstrap.css">
</head>
<body bg-secondary>
	<h1 class="text-center text-white  my-4">Welcome Zodiac Arrest!</h1>
	<div class="col-lg-4 offset-lg-4">
		<form class="p-4" action="controllers/zodiac-registration.php" method="POST">
			<div class="form-group">
				<label for="name" class="text-white">Name:</label>
				<input type="text" name="name" class="form-control"> 
			</div>
			<div class="form-group">
				<label for="birthMonth" class="text-white">Birth Month</label>
				<input type="number" name="birthMonth" class="form-control">
			</div>
			<div class="form-group">
				<label for="birthDay" class="text-white">Birth Day</label>
				<input type="number" name="birthDay" class="form-control">
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-success">Check Zodiac</button>
			</div>
		</form>
	</div>
</body>
</html>